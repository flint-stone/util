// Package pipelining provides a pipeline definition.
package pipelining

import (
	"reflect"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/util"
	"gitlab.com/akita/util/tracing"
)

type PipelineItem interface {
	TaskID() string
}

// Pipeline allows simulation designers to define pipeline structures.
type Pipeline interface {
	tracing.NamedHookable

	// Tick moves elements in the pipeline forward.
	Tick(now akita.VTimeInSec) (madeProgress bool)

	// CanAccept checks if the pipeline can accept a new element.
	CanAccept() bool

	// Accept adds an element to the pipeline. If the first pipeline stage is
	// currently occupied, this function panics.
	Accept(now akita.VTimeInSec, elem PipelineItem)
}

// NewPipeline creates a default pipeline
func NewPipeline(
	name string,
	numStage, cyclePerStage int,
	postPipelineBuf util.Buffer,
) Pipeline {
	p := &pipelineImpl{
		numStage:        numStage,
		cyclePerStage:   cyclePerStage,
		postPipelineBuf: postPipelineBuf,
	}

	p.stages = make([]pipelineStageInfo, numStage)

	return p
}

type pipelineStageInfo struct {
	elem      PipelineItem
	cycleLeft int
}

type pipelineImpl struct {
	akita.HookableBase
	name            string
	numStage        int
	cyclePerStage   int
	postPipelineBuf util.Buffer
	stages          []pipelineStageInfo
}

func (p *pipelineImpl) Name() string {
	return p.name
}

// Tick moves elements in the pipeline forward.
func (p *pipelineImpl) Tick(now akita.VTimeInSec) (madeProgress bool) {
	for i := p.numStage - 1; i >= 0; i-- {
		stage := &p.stages[i]

		if stage.elem == nil {
			continue
		}

		if stage.cycleLeft > 0 {
			stage.cycleLeft--
			madeProgress = true
			continue
		}

		if i == p.numStage-1 {
			madeProgress =
				p.tryMoveToPostPipelineBuffer(now, stage) || madeProgress
		} else {
			madeProgress = p.tryMoveToNextStage(i) || madeProgress
		}
	}

	return madeProgress
}

func (p *pipelineImpl) tryMoveToPostPipelineBuffer(
	now akita.VTimeInSec,
	stage *pipelineStageInfo,
) (succeed bool) {
	if !p.postPipelineBuf.CanPush() {
		return false
	}

	tracing.EndTask(stage.elem.TaskID()+"pipeline", now, p)

	p.postPipelineBuf.Push(stage.elem)
	stage.elem = nil

	return true
}

func (p *pipelineImpl) tryMoveToNextStage(stageNum int) (succeed bool) {
	stage := &p.stages[stageNum]
	nextStage := &p.stages[stageNum+1]
	if nextStage.elem != nil {
		return false
	}

	nextStage.elem = stage.elem
	nextStage.cycleLeft = p.cyclePerStage - 1
	stage.elem = nil
	return true
}

// CanAccept checks if the pipeline can accept a new element.
func (p *pipelineImpl) CanAccept() bool {
	return p.stages[0].elem == nil
}

// Accept adds an element to the pipeline. If the first pipeline stage is
// currently occupied, this function panics.
func (p *pipelineImpl) Accept(now akita.VTimeInSec, elem PipelineItem) {
	if p.stages[0].elem != nil {
		panic("Cannot push to pipeline")
	}

	p.stages[0].elem = elem
	p.stages[0].cycleLeft = p.cyclePerStage - 1

	tracing.StartTask(
		elem.TaskID()+"pipeline",
		elem.TaskID(),
		now,
		p,
		"pipeline",
		reflect.TypeOf(elem).String(),
		nil,
	)
}
